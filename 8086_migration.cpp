#define _CRT_SECURE_NO_WARNINGS
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <stdint.h>
#include <cctype>

void splitstring(std::string& s1, std::string& s2, std::string delimiter) //split s2 into s1 and s2
{
	size_t pos = 0;
	pos = s2.find(delimiter);
	s1 = s2.substr(0, pos);
	s2.erase(0, pos + delimiter.length());
}

char address_calc[8][8] = { "bx + si",
							"bx + di",
							"bp + si",
							"bp + di",
							"si",
							"di",
							"bp",
							"bx" };

char registers[2][8][3] = { {"al", "cl", "dl", "bl", "ah", "ch", "dh", "bh"},
							{"ax", "cx", "dx", "bx", "sp", "bp", "si", "di"} };

char cond_jumps[16][5] = { "jo", "jno", "jb", "jnb", "je", "jnz", "jbe", "ja", 
							"js", "jns", "jp", "jnp", "jl", "jnl", "jle", "jg"};

char other_cond_jumps[4][7] = { "loopnz", "loopz", "loop", "jcxz" };

void strcopy(char* s, char* t) // copy t to s
{
	while (*s++ = *t++);
}

void mod_reg_r_m(unsigned int byte_1, std::ifstream& in, char* dest, char* source)
{
	uint8_t d = (byte_1 & ((1 << 2) - 1)) >> 1; // Keep last 2 then discard last
	uint8_t w = byte_1 & ((1 << 1) - 1);
	uint8_t byte_2 = in.get();
	uint8_t MOD = byte_2 >> 6;
	uint8_t reg = (byte_2 & ((1 << 6) - 1)) >> 3;
	uint8_t r_m = byte_2 & ((1 << 3) - 1);
	switch (MOD)
	{
	case 0b11:
	{
		if (d == 1)
		{
			strcopy(dest, registers[w][reg]);
			strcopy(source, registers[w][r_m]);
		}
		else
		{
			strcopy(source, registers[w][reg]);
			strcopy(dest, registers[w][r_m]);
		}

	} break;

	case 0b00:
	{
		if (r_m == 0b110)
		{
			short data = in.get();
			data += in.get() << 8;
			if (d == 1)
			{
				strcopy(dest, registers[w][reg]);
				int n = sprintf(source, "[%d]", data);
			}
			else
			{
				strcopy(source, registers[w][reg]);
				int n = sprintf(dest, "[%s]", address_calc[r_m]);
			}
		}
		else
		{
			if (d == 1)
			{
				strcopy(dest, registers[w][reg]);
				int n = sprintf(source, "[%s]", address_calc[r_m]);
			}
			else
			{
				strcopy(source, registers[w][reg]);
				int n = sprintf(dest, "[%s]", address_calc[r_m]);
			}
		}

	} break;

	default: // 0b01 or 0b10
	{
		short byte_3 = in.get();
		if (MOD == 0b10)
		{
			byte_3 += in.get() << 8;
		}
		else
		{
			byte_3 = (char)byte_3;
		}
		if (d == 1)
		{
			strcopy(dest, registers[w][reg]);
			if (byte_3)
			{
				if (byte_3 > 0)
				{
					int n = sprintf(source, "[%s + %d]", address_calc[r_m], byte_3);
				}
				else
				{
					int n = sprintf(source, "[%s %d]", address_calc[r_m], byte_3);
				}
			}
			else
			{
				int n = sprintf(source, "[%s]", address_calc[r_m]);
			}
		}
		else
		{
			strcopy(source, registers[w][reg]);
			if (byte_3)
			{
				if (byte_3 > 0)
				{
					int n = sprintf(dest, "[%s + %d]", address_calc[r_m], byte_3);
				}
				else
				{
					int n = sprintf(dest, "[%s %d]", address_calc[r_m], byte_3);
				}
			}
			else
			{
				int n = sprintf(dest, "[%s]", address_calc[r_m]);
			}
		}
	} break;
	}
}

uint8_t mod_r_m_immediate(unsigned int byte_1, std::ifstream& in, char* dest)
{
	uint8_t byte_2 = in.get();
	uint8_t MOD = byte_2 >> 6;
	uint8_t r_m = byte_2 & 7;
	short byte_3;
	short data;

	if (MOD == 0b01)
	{
		byte_3 = in.get();
		byte_3 = char(byte_3);
		if (byte_3 > 0)
		{
			int n = sprintf(dest, "[%s + %d]", address_calc[r_m], byte_3);
		}
		else
		{
			int n = sprintf(dest, "[%s %d]", address_calc[r_m], byte_3);
		}
	}
	else if (MOD == 0b10)
	{
		byte_3 = in.get();
		byte_3 += in.get() << 8;
		if (byte_3 > 0)
		{
			int n = sprintf(dest, "[%s + %d]", address_calc[r_m], byte_3);
		}
		else
		{
			int n = sprintf(dest, "[%s %d]", address_calc[r_m], byte_3);
		}
	}
	else if (MOD == 0)
	{
		if (r_m == 0b110)
		{
			data = in.get();
			data += in.get() << 8;
			int n = sprintf(dest, "[%d]", data);
		}
		else
		{
			int n = sprintf(dest, "[%s]", address_calc[r_m]);
		}
	}

	else // MOD == 0b11
	{
		strcopy(dest, registers[byte_1 & 1][r_m]);
	}

	return (byte_2 >> 3) & 7;
}

std::string disassemble(std::ifstream& in)
{
	std::string output;
	/*std::ifstream in;
	in.open(infile, std::ifstream::binary);*/

	output += "bits 16\n";
	unsigned int byte_1;

	while ((byte_1 = in.get()) != EOF)
	{	
		if ((byte_1 >> 2) == 0b100010) //reg/mem to from reg
		{
			char dest[20];
			char source[20];

			mod_reg_r_m(byte_1, in, dest, source);
			
			output += "\nmov ";
			output += dest;
			output += ", ";
			output += source;
		}
		else if ((byte_1 >> 4) == 0b1011) //immediate to reg
		{
			uint8_t w = (byte_1 >> 3) & 1;
			uint8_t reg = byte_1 & 7;
			short data = in.get();
			if (w)
			{
				data += in.get() << 8;
			}
			else
			{
				data = char(data);
			}
			output += "\nmov ";
			output += registers[w][reg];
			output += ", ";
			output += std::to_string(data); 
		}

		else if ((byte_1 >> 1) == 0b1100011) // immediate to reg/memory
		{
			char dest[20];
			char source[20];
			uint8_t w = byte_1 & 1;
			uint8_t _ = mod_r_m_immediate(byte_1, in, dest);

			short data = in.get();
			if (w)
			{
				data += in.get() << 8;
				int n = sprintf(source, "word %d", data);
			}
			else
			{
				data = (char)data;
				int n = sprintf(source, "byte %d", data);
			}

			output += "\nmov ";
			output += dest;
			output += ", ";
			output += source;
		}

		else if ((byte_1 >> 2) == 0b101000) //acc
		{
			uint16_t addr = in.get();
			addr += in.get() << 8;
			if ((byte_1 & 3) >> 1)
			{
				output += "\nmov [";
				output += std::to_string(addr);
				output += "], ax";
			}
			else
			{
				output += "\nmov ax, [";
				output += std::to_string(addr);
				output += "]";
			}
		}

		else if (  (byte_1 >> 2) == 0
			|| (byte_1 >> 2) == 0b001010
			|| (byte_1 >> 2) == 0b001110) // add/sub/cmp reg/mem to either
		{
			char dest[20];
			char source[20];

			mod_reg_r_m(byte_1, in, dest, source);
			uint8_t op = (byte_1 >> 3) & 7;
			switch (op)
			{
				case 0b000:
				{
					output += "\nadd ";
				} break;
				case 0b101:
				{
					output += "\nsub ";
				} break;
				case 0b111:
				{
					output += "\ncmp ";
				} break;
				default:
				{
					;
				} break;
			}
			output += dest;
			output += ", ";
			output += source;
		}
		
		else if ((byte_1 >> 2) == 0b100000) // add/sub/cmp immediate, reg/mem
		{
			char dest[20];
			char source[20];
			uint8_t s = (byte_1 >> 1) & 1;
			uint8_t w = byte_1 & 1;
			uint8_t op = mod_r_m_immediate(byte_1, in, dest);

			short data = in.get();
			
			if (!s && w)
			{
				data += in.get() << 8;
				int n = sprintf(source, "%d", data);
			}
			else
			{
				data = (char)data;
				int n = sprintf(source, "%d", data);
			}

			switch (op)
			{
			case 0b000:
			{
				output += "\nadd ";
			} break;
			case 0b101:
			{
				output += "\nsub ";
			} break;
			case 0b111:
			{
				output += "\ncmp ";
			} break;
			default:
			{
				;
			} break;
			}

			if (dest[0] == '[') 
			{
				if (w)
				{
					output += "word ";
				}
				else
				{
					output += "byte ";
				}
			}
			output += dest;
			output += ", ";
			output += source;
		}
		
		else if (!(byte_1 >> 6) && (((byte_1 >> 1) & 2) == 0b10)) //add/sub/cmp to acc
		{
			uint8_t w = byte_1 & 1;
			uint8_t op = (byte_1 >> 3) & 7;
			short data = in.get();
			if (w)
			{
				data += in.get() << 8;
				switch (op)
				{
				case 0b000:
				{
					output += "\nadd ax, ";
				} break;
				case 0b101:
				{
					output += "\nsub ax, ";
				} break;
				case 0b111:
				{
					output += "\ncmp ax, ";
				} break;
				default:
				{
					;
				} break;
				}
			}
			else
			{
				data = char(data);
				switch (op)
				{
				case 0b000:
				{
					output += "\nadd al, ";
				} break;
				case 0b101:
				{
					output += "\nsub al, ";
				} break;
				case 0b111:
				{
					output += "\ncmp al, ";
				} break;
				default:
				{
					;
				} break;
				}
			}
			
			output += std::to_string(data);
		}

		else if ((byte_1 >> 4) == 0b0111) //cond jumps
		{
			uint8_t op = byte_1 & 15;
			output += "\n";
			output += cond_jumps[op];
			output += " $ ";
			int8_t offset = in.get();
			if (offset >= -2)
			{
				output += "+";
			}
			output += std::to_string(offset+2);
		}

		else if ((byte_1 >> 4) == 0b1110) // other cond jumps
		{
			uint8_t op = byte_1 & 3;
			output += "\n";
			output += other_cond_jumps[op];
			output += " $ ";
			int8_t offset = in.get();
			if (offset >= -2)
			{
				output += "+";
			}
			output += std::to_string(offset + 2);
		}

		else
		{
		printf("Behaviour not coded yet");
		}
	}
	return output+"\n";
}

void simulate_mov(std::string instruction, int register_values[8], char register_names[8][3])
{
	std::string dest = "";
	splitstring(dest, instruction, ", ");
	int dest_index = 0;
	int source_index = 0;
	bool dest_low = false;
	bool dest_high = false;
	int value = 0;
	while (dest_index < 11 && register_names[dest_index] != dest)
	{
		dest_index++;
	}

	if (dest_index == 11)
	{
		dest_index = 0;
		while (dest_index < 8 && registers[1][dest_index] != dest)
		{
			dest_index++;
		}
		if (dest_index == 8)
		{
			std::cout << "mov: couldn't find dest register" << std::endl;
		}
		else if (dest_index < 4)
		{
			dest_low = true;
		}
		else
		{
			dest_high = true;
			dest_index -= 4;
		}
	}

	if (isdigit(instruction[0]))
	{
		value = std::stoi(instruction);
	}
	else if (instruction[0] != '[') // from register
	{
		while (source_index < 11 && register_names[source_index] != instruction)
		{
			source_index++;
		}

		if (source_index == 11)
		{
			source_index = 0;
			while (source_index < 8 && registers[1][source_index] != instruction)
			{
				source_index++;
			}
			if (source_index == 8)
			{
				std::cout << "mov: couldn't find source register" << std::endl;
			}
			else if (source_index < 4)
			{
				value = register_values[source_index] & 0x0011;
			}
			else
			{
				source_index -= 4;
				value = register_values[source_index] >> 8;
			}
		}
		else
		{
			value = register_values[source_index];
		}
	}

	register_values[dest_index] = value;
}

std::string simulate(std::ifstream& in)
{
	int register_values[11] = {};
	char register_names[11][3] = { "ax", "bx", "cx", "dx", "sp", "bp", "si", "di", "es", "ss", "ds" };
	
	char c;
	std::string line;
	std::getline(in, line);
	std::getline(in, line);
	std::getline(in, line);

	for (;line != "";std::getline(in,line))
	{
		size_t pos = 0;
		std::string op = "";
		splitstring(op, line, " ");

		if (op == "mov")
		{
			simulate_mov(line, register_values, register_names);
		}
	}

	std::string output = "";

	for (int i = 0; i < 8; i++)
	{
		output = output + register_names[i] + ": " + std::to_string(register_values[i]) + '\n';
	}

	return output;

}

int main(int ArgCount, char** Args)
{
	bool sim = false;
	if (ArgCount > 1)
	{
		for (int ArgIndex = 1; ArgIndex < ArgCount; ArgIndex++)
		{
			std::string arg = Args[ArgIndex];
			if (arg == "-exec")
			{
				sim = true;
				continue;
			}
			std::ifstream input;
			input.open(Args[ArgIndex], std::ifstream::binary);
			std::ofstream output;
			std::string o = Args[ArgIndex];
			std::string out = o + "decoded.asm";
			output.open(out);
			output << disassemble(input);
			output.close();
			
			if (sim)
			{
				input.close();
				input.open(out, std::ifstream::in);
				if (!input.good())
				{
					std::cout << "File error" << std::endl;
				}
				out = o + "simulated.txt";
				output.open(out);
				output << simulate(input);
				output.close();
			}
		}
	}
	

}