#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdint.h>

char registers[2][8][3] = { {"al", "cl", "dl", "bl", "ah", "ch", "dh", "bh"}, 
							{"ax", "cx", "dx", "bx", "sp", "bp", "si", "di"}};

char address_calc[8][8] = { "bx + si",
							"bx + di",
							"bp + si",
							"bp + di",
							"si",
							"di",
							"bp",
							"bx" };

void strcopy(char* s, char* t) // copy t to s
{
	while (*s++ = *t++);
}

void disassemble(FILE* in, FILE* out)
{
	fprintf(out, "bits 16\n");
	unsigned int byte_1;

	while ((byte_1 = fgetc(in)) != EOF)
	{
		if ((byte_1 >> 2) == 0b100010) //reg/mem to from reg
		{
			uint8_t d = (byte_1 & ((1 << 2) - 1)) >> 1; // Keep last 2 then discard last
			uint8_t w = byte_1 & ((1 << 1) - 1);
			uint8_t byte_2 = fgetc(in);
			uint8_t MOD = byte_2 >> 6;
			uint8_t reg = (byte_2 & ((1 << 6) - 1)) >> 3;
			uint8_t r_m = byte_2 & ((1 << 3) - 1);
			char dest[20];
			char source[20];
			switch (MOD)
			{
			case 0b11:
			{
				if (d == 1)
				{
					strcopy(dest, registers[w][reg]);
					strcopy(source, registers[w][r_m]);
				}
				else
				{
					strcopy(source, registers[w][reg]);
					strcopy(dest, registers[w][r_m]);
				}

			} break;

			case 0b00:
			{
				if (r_m == 0b110)
				{
					short data = fgetc(in);
					data += fgetc(in) << 8;
					if (d == 1)
					{
						strcopy(dest, registers[w][reg]);
						int n = sprintf(source, "[%d]", data);
					}
					else
					{
						strcopy(source, registers[w][reg]);
						int n = sprintf(dest, "[%s]", address_calc[r_m]);
					}
				}
				else
				{
					if (d == 1)
					{
						strcopy(dest, registers[w][reg]);
						int n = sprintf(source, "[%s]", address_calc[r_m]);
					}
					else
					{
						strcopy(source, registers[w][reg]);
						int n = sprintf(dest, "[%s]", address_calc[r_m]);
					}
				}

			} break;

			default: // 0b01 or 0b10
			{
				short byte_3 = fgetc(in);
				if (MOD == 0b10)
				{
					byte_3 += fgetc(in) << 8;
				}
				else
				{
					byte_3 = (char)byte_3;
				}
				if (d == 1)
				{
					strcopy(dest, registers[w][reg]);
					if (byte_3)
					{
						if (byte_3 > 0)
						{
							int n = sprintf(source, "[%s + %d]", address_calc[r_m], byte_3);
						}
						else
						{
							int n = sprintf(source, "[%s %d]", address_calc[r_m], byte_3);
						}
					}
					else
					{
						int n = sprintf(source, "[%s]", address_calc[r_m]);
					}
				}
				else
				{
					strcopy(source, registers[w][reg]);
					if (byte_3)
					{
						if (byte_3 > 0)
						{
							int n = sprintf(dest, "[%s + %d]", address_calc[r_m], byte_3);
						}
						else
						{
							int n = sprintf(dest, "[%s %d]", address_calc[r_m], byte_3);
						}
					}
					else
					{
						int n = sprintf(dest, "[%s]", address_calc[r_m]);
					}
				}
			} break;
			}
			fprintf(out, "mov %s, %s\n", dest, source);
		}
		else if ((byte_1 >> 4) == 0b1011) //immediate to reg
		{
			uint8_t w = (byte_1 >> 3) & 1;
			uint8_t reg = byte_1 & 7;
			short data = fgetc(in);
			if (w)
			{
				data += fgetc(in) << 8;
			}
			else
			{
				data = char(data);
			}
			fprintf(out, "mov %s, %d\n", registers[w][reg], data);
		}

		else if ((byte_1 >> 1) == 0b1100011) // immediate to reg/memory
		{
			uint8_t w = byte_1 & 1;
			uint8_t byte_2 = fgetc(in);
			uint8_t MOD = byte_2 >> 6;
			uint8_t r_m = byte_2 & 7;
			char dest[20];
			char source[20];
			short byte_3;
			short data;

			if (MOD == 0b01)
			{
				byte_3 = fgetc(in);
				byte_3 = char(byte_3);
				if (byte_3 > 0)
				{
					int n = sprintf(dest, "[%s + %d]", address_calc[r_m], byte_3);
				}
				else
				{
					int n = sprintf(dest, "[%s %d]", address_calc[r_m], byte_3);
				}
			}
			else if (MOD == 0b10)
			{
				byte_3 = fgetc(in);
				byte_3 += fgetc(in) << 8;
				if (byte_3 > 0)
				{
					int n = sprintf(dest, "[%s + %d]", address_calc[r_m], byte_3);
				}
				else
				{
					int n = sprintf(dest, "[%s %d]", address_calc[r_m], byte_3);
				}
			}
			else if (MOD == 0)
			{
				if (r_m == 0b110)
				{
					data = fgetc(in);
					data += fgetc(in) << 8;
					int n = sprintf(dest, "[%d]", data);
				}
				else
				{
					int n = sprintf(dest, "[%s]", address_calc[r_m]);
				}
			}
			data = fgetc(in);
			if (w)
			{
				data += fgetc(in) << 8;
				int n = sprintf(source, "word %d", data);
			}
			else
			{
				data = (char)data;
				int n = sprintf(source, "byte %d", data);
			}

			fprintf(out, "mov %s, %s\n", dest, source);
		}

		else if ((byte_1 >> 2) == 0b101000) //acc
		{
			uint16_t addr = fgetc(in);
			addr += fgetc(in) << 8;
			if ((byte_1 & 3) >> 1)
			{
				fprintf(out, "mov [%d], ax\n", addr);
			}
			else
				fprintf(out, "mov ax, [%d]\n", addr);
		}

		else
		{
			printf("Behaviour not coded yet");
		}
	}
}

int main()
{
	/*FILE* in_37 = fopen("../part1/listing_0037_single_register_mov", "r");
	FILE* out_37 = fopen("../part1/37.asm", "w");
	disassemble(in_37, out_37);
	FILE* in_38 = fopen("../part1/listing_0038_many_register_mov", "r");
	FILE* out_38 = fopen("../part1/38.asm", "w");
	disassemble(in_38, out_38);
	FILE* in_39 = fopen("../part1/listing_0039_more_movs", "r");
	FILE* out_39 = fopen("../part1/39.asm", "w");
	disassemble(in_39, out_39);*/
	FILE* in_40 = fopen("../part1/listing_0040_challenge_movs", "r");
	FILE* out_40 = fopen("../part1/40.asm", "w");
	disassemble(in_40, out_40);
}